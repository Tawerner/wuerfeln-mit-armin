FROM python:3.9-alpine

EXPOSE 5000

# Keeps Python from generating .pyc files in the container
ENV PYTHONDONTWRITEBYTECODE=1
# Turns off buffering for easier container logging
ENV PYTHONUNBUFFERED=1

COPY requirements.txt /tmp/
RUN python -m pip install -r /tmp/requirements.txt

RUN adduser -S appuser
USER appuser

COPY src/ /app/

CMD ["python", "/app/api.py"]