import string
import random

class WuerfelnMitArmin:
    text = (
        'Was wir *jetzt* brauchen, ist ein/e $quantifier-$timespan $adjective1 $subject1a-$subject1b'
        ' bis $until zur $adjective2 $substantive der $subject2.'
    )
    words = {
        'quantifier': random.choice([
            'ein', 'zwei', 'drei', 'vier', 'fünf', 'sechs',
        ]),
        'timespan': random.choice([
            'tägige/n', 'wöchige/n', 'monatige/n', 'fache/n', 'malige/n', 'hebige/n',
        ]),
        'adjective1': random.choice([
            'harte/n', 'soften', 'optionale/n', 'intransparente/n', 'alternativlose/n',
            'unumkehrbare/n',
        ]),
        'subject1a': random.choice([
            'Wellenbrecher', 'Brücken', 'Treppen', 'Wende', 'Impf', 'Ehren',
        ]),
        'subject1b': random.choice([
            'Lockdown', 'Stopp', 'Maßnahme', 'Kampagne', 'Sprint', 'Matrix',
        ]),
        'until': random.choice([
            'zum Sommer', 'auf Weiteres', 'zur Bundestagswahl', '2030',
            'nach den Abiturprüfungen', 'in die Puppen',
        ]),
        'adjective2': random.choice([
            'sofortigen', 'nachhaltigen', 'allmählichen', 'unausweichlichen',
            'wirtschaftsschonenden', 'willkürlichen',
        ]),
        'substantive': random.choice([
            'Senkung', 'Steigerung', 'Beendigung', 'Halbierung', 'Vernichtung', 'Beschönigung',
        ]),
        'subject2': random.choice([
            'Infektionszahlen', 'privaten Treffen', 'Wirtschaftsleistung', 'Wahlprognosen',
            'dritten Welle', 'Bundeskanzlerin',
        ]),
    }

    @classmethod
    def quoteRandom(cls):
        return(string.Template(cls.text).substitute(cls.words))

if __name__ == '__main__':
    print(WuerfelnMitArmin.quoteRandom())