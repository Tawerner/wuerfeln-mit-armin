import flask
import flask_restful
import wuerfeln_mit_armin

app = flask.Flask(__name__)
api = flask_restful.Api(app, prefix='/api/v1')

class QuoteRandom(flask_restful.Resource):
    def get(self):
        return { 'quote': wuerfeln_mit_armin.WuerfelnMitArmin.quoteRandom() }

api.add_resource(QuoteRandom, '/quoteRandom')

if __name__ == '__main__':
    app.run(host='0.0.0.0')